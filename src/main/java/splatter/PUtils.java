/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package splatter;

/**
 *
 * @author Yousif
 */
public class PUtils {

    public static int red(int c) {
        return (c & 0x00FF0000) >> 16;
    }

    public static int green(int c) {
        return (c & 0x0000FF00) >> 8;
    }

    public static int blue(int c) {
        return (c & 0x000000FF);
    }
}
