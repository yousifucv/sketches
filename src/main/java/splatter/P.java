/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package splatter;

import processing.core.PApplet;
import processing.core.PVector;

/*
 *
 * @author Yousif
 */
public class P extends PApplet {

	public static PApplet P;

	static int SCREEN_WIDTH = 0;
	static int SCREEN_HEIGHT = 0;
	static boolean isFullScreen = false;
	static float resRatio = 0.8f;

	static long seed;

	public static void main(String args[]) {
		if (isFullScreen) {
			PApplet.main(new String[]{"--present", "splatter.P"});
		} else {
			PApplet.main(new String[]{"splatter.P"});
		}
	}
	
	public void settings() {
		P = this;
		if (isFullScreen) {
			size(displayWidth, displayHeight, P2D);
		} else {
			size((int)(displayWidth * resRatio), (int)(displayHeight * resRatio), P3D);
		}
		smooth(0);
		seed = millis();
		//setup() starts here
	}

	public void setup() {

	}

	@Override
	public void draw() {
		//draw() starts here
		if(frameCount==1) {
		}
		background(255);
		randomSeed(seed);
		int diameter = 200;
		for(int x = diameter/2; x < width - diameter/2; x+=diameter) {
			for(int y = diameter/2; y < height - diameter/2; y+=diameter) {
				drawSplatter(new PVector(x,y), diameter);
			}
		}
//		drawSplatter(new PVector(300,300), 100);
//		drawSplatter(new PVector(100,200), 50);
//		drawSplatter(new PVector(500,100), 200);


	}

	public void drawSplatter(PVector point, int diameter) {

		//for (int y = p.y - d/2; y <= p.y + d/2; y++)

		float maxDistance = diameter * 0.8f / 2;
		float maxAverageDistance = diameter * map(mouseX, 0, width, 0, 1.0f) / 2;
 
		//create point array, randomly placed within the circle with radius maxDistance
		//the number of points is proportional to the radius (minimum of 3 points)
		int pointNum = (int) max(pow(maxDistance, 1.5f) * 0.05f, 3);
		PVector[] pointArray = new PVector[pointNum];
		for(int i = 0; i < pointArray.length; i++) {
			float randomAngle = random(TWO_PI);
			float randomRadius = random(maxDistance);
			PVector random = new PVector(randomRadius*cos(randomAngle), randomRadius*sin(randomAngle));
			pointArray[i] = new PVector(random.x,random.y);
		}

		float diagonal = PVector.dist(new PVector(0,0), new PVector(diameter/2,diameter/2));
		
		//only odd numbers for diameter
		for(int j = -diameter/2; j <= diameter/2; j++) {
			for(int i = -diameter/2; i <= diameter/2; i++) {
				PVector pixelLocation = new PVector(i, j);
				float distance = PVector.dist(new PVector(0,0), pixelLocation);
				//getting the average dist to each of the points
				float sum = 0;
				float average = 0;
				float c = (diagonal - distance) * 1f;
				for(int k = 0; k < pointArray.length; k ++) {
					sum += c / pow(PVector.dist(pointArray[k], pixelLocation), 2f);
				}
				average = sum / pointArray.length;
				float threshold = map(mouseY, 0, height, 0, 50);
				if(sum > threshold) {
					stroke(computeColor(0xFFFFFFFF, 0xFF000000, sum, threshold, threshold + 1));
					strokeWeight(1);
					point(point.x + i + 0.5f, point.y + j + 0.5f); //must add 0.5f because there's no smoothing, +0.5 means we are pointing to the centre of the pixel
				}
			}
		}

		//drawing points
		for(int i = 0; i < pointArray.length; i++) {
			stroke(255,0,0);
			strokeWeight(1);
			point(point.x + pointArray[i].x, point.y + pointArray[i].y);
		}
		noFill();
		rectMode(CENTER);
		rect(point.x,point.y,diameter, diameter);

	}

	public int computeColor(int color1, int color2, float value, float min, float max) {
		float t = map(value, min, max, 0, 1);
		return lerpColor(color1, color2, t);
	}







	//prints framerate in the top left
	void showFrameRate() {
		camera(width / 2, height / 2, (height / 2) / tan(PI / 6), width / 2, height / 2, 0, 0, 1, 0);
		fill(125);
		text(frameRate, 20, 20);
	}

	//draws a small square mouseCursor
	void updateMouse() {
		camera(width / 2, height / 2, (height / 2) / tan(PI / 6), width / 2, height / 2, 0, 0, 1, 0);
		noStroke();
		fill(125, 125, 125, 100);
		rect(mouseX - 2, mouseY - 2, 4, 4);
	}

	//draws an axis with a specified length, and location
	void drawAxis(int length, int x, int y, int z) {
		translate(x, y, z);
		drawAxis(length);
		translate(-x, -y, -z);
	}

	//draws an axis with specified length
	private void drawAxis(int length) {
		stroke(255, 0, 0);
		fill(255, 0, 0);
		line(-length / 2, 0, 0, length / 2, 0, 0);//xaxis
		translate(length / 2, 0, 0);
		fill(255,100,100);
		box(10, 10, 10);
		translate(-length / 2, 0, 0);

		stroke(0, 255, 0);
		fill(0, 255, 0);
		line(0, -length / 2, 0, 0, length / 2, 0);//yaxis
		translate(0, length / 2, 0);
		fill(200,255,200);
		box(10, 10, 10);
		translate(0, -length / 2, 0);

		stroke(0, 0, 255);
		fill(0, 0, 255);
		line(0, 0, -length / 2, 0, 0, length / 2);//zaxis
		translate(0, 0, length / 2);
		fill(100,100,255);
		box(10, 10, 10);
		translate(0, 0, -length / 2);
	}

}
