package smokeribbon;

import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;

public class P extends PApplet {
	
	public static void main(String args[]) {
		PApplet.main(new String[]{"smokeribbon.P"});
	}
	
	public void settings() {
		  size(500, 500, P2D); 
		  smooth(4);
	}
	
	public void setup() {
		  background(0);

		  rootNode = new RootNode();
		  rootNode.position = new PVector(250, 250);
		  generateRootNode();
	}

	RootNode rootNode;
	Node node;

	public void draw() {
	  rootNode.draw();
	  if (mousePressed && mouseButton == LEFT) {
	    background(0);
	  }

	}

	public void generateRootNode() {
	    rootNode.childNodes = makeRandomChildNodes(2, 4, 3);
	}

	class RootNode {
	  PVector position;
	  ArrayList<Node> childNodes;
	  void draw() {
	    float follow = 0.05f;
	    position.x = lerp(position.x, mouseX, follow);
	    position.y = lerp(position.y, mouseY, follow);
	    translate(position.x, position.y);
	    //translate(mouseX, mouseY);
	    for (Node n : childNodes) {
	      n.draw();
	    }
	  }
	}

	class Node {
	  float orbitalDistance;
	  float angularSpeed;
	  float angle;
	  int col;
	  int size;
	  ArrayList<Node> childNodes;
	  boolean visible = true;
	  void draw() {
	    pushMatrix();
	    stroke(255);
	    strokeWeight(1);
	    rotate(angle);
	    strokeWeight(size);
	    stroke(col);
	    translate(orbitalDistance, 0);
	    if (visible) point(0, 0);
	    for (Node n : childNodes) {
	      n.draw();
	    }
	    angle += angularSpeed;
	    orbitalDistance *= 0.995f;
	    popMatrix();
	  }
	}
	public void mousePressed() {
	  if(mouseButton == RIGHT)
	    generateRootNode();
	}
	public void mouseReleased() {
	  if(mouseButton == RIGHT)
	  rootNode.childNodes = new ArrayList<Node>();
	}

	public ArrayList<Node> makeRandomChildNodes(int minimum, int maximum, int depth) {

	  int num = (int) random(minimum, maximum + 1);
	  ArrayList<Node> nodes = new ArrayList<Node>();
	  if (depth == 0) return nodes;
	  float randomAngle = random(0, TAU);
	  float angularSpeed = random(0.01f, 0.02f);
	  for (int i = 0; i < num; i++) {
	    Node node = new Node();
	    node.orbitalDistance = random(20, 80);
	    node.angularSpeed =  random(-0.01f, 0.01f) * 2;
	    node.angle = random(0, TAU);    

	    if (depth == 1) {
	      node.col = color(random(256), random(256), random(256), 0x40);
	      node.size = 2;
	      node.visible = false;
	      node.childNodes = makeStraightChild(5);
	    } else {
	      node.size = 2;
	      node.col = 0x20FF0000;
	      node.childNodes = makeRandomChildNodes(minimum, maximum, depth - 1);
	    }

	    nodes.add(node);
	  }
	  return nodes;
	}

	public ArrayList<Node> makeStraightChild(int num) {

	  if (num % 2 == 1) num++;

	  ArrayList<Node> nodes = new ArrayList<Node>();

	  float angularSpeed = random(-0.05f, 0.05f);
	  for (int i = 0; i < num; i++) {
	    Node node = new Node();
	    node.orbitalDistance = ((i / (float)(num - 1)) - .5f) * 20 ;
	    node.angularSpeed = angularSpeed;
	    node.angle = 0;
	    node.col = 0x20FFFFFF;
	    node.size = 1;
	    node.childNodes = new ArrayList<Node>();
	    nodes.add(node);
	  }
	  return nodes;
	}
}
